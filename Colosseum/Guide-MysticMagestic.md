# General - Mystic Magistic�s Fun and Simple Colo Guide for 5+ Mage Fellows

This guide is based upon �Shedao Shai's Colosseum Walkthrough - Standard Fellow.�  There are some differences that incorporate observations for running a fellow with 5+ mages.  This callout method has generated runs that complete with 15 minutes + left on the ticket � enough time for all mini-bosses, including The Master.  It�s also been successfully used (including the controversial Soulbound on Virindi�s and Olthoi) for wins with a 6-man fellow leaving over 8 minutes to spare.   
Please keep in mind as you take on the Colo challenges that success, like most things in life, is based upon having a coordinated group.  I hope you have fun!   
   
## Quick Pre-Game Summary � questions generally copied into fellow chat prior to run   
   
Key Roles � who will do the Imperils?  
Ok � we only need Imperils in rooms where you see the callout � watch for IMPERILS in room messages please.  
Who can assist doing vulns on Carenzi & TM�s?  
Great! TM�s need both bludg & frost.  Guys unless something odd happens, Virindi do NOT need vulns (we�ll see how it goes)  
General approach � MAGES � please watch for STREAK request� generally <4 MOBS always use streaks.  
Use your judgment if using Rings or TF�s to help with kills � we�ve missed �fast room clears� because of these spells, where the streak approach with proper weapon maximizes our efficiency  
Why? Consider the casting time for Rings, TF�s and Arc�s� if you spend that 3 to 4 seconds on a cast to potentially miss a moving target OR cast on a target that dies before your spell lands, that�s wasted damage.  
Streaks are super fast and optimize the targeting pattern for high DPS groups.  
We�ve had winning runs where streaks with a high damage or Soulbound weapon are used 95% of the time.  
Important Note � please use your Soulbound Weapons on Virindi � time wasted on vulns (and resists) leads to longer runs: kill, kill, kill!  
We�re going to win this either way, we�re a strong group � however, I�d appreciate if you�d try it by the callout� I think you�ll like it.  
For doubters about no Vslayers / Vulns, the Soulbound with a 21 damage rating can crit a virindi for over 2600 � streaks over 1400 on crit!  
Why? With 12 to 15 mobs in the room and each vuln taking about 4 seconds on average (with S2M�s included in count) � we lose a fellow that could be creating damage / kills for about 1M as they vuln: the small (if any) VSlayer damage benefit advantage does not justify trading 1/9th (11%) of our damage potential for a non-damage attack.  
Imperils and Soulbounds are all we need.  
Remember the approach that makes you effective hunting solo may need to be adjusted to be a good Colo teammate� the SB request is one of those things.  Thanks.  
Any questions?  


### Arena 6
6a�- Mosswarts -�Ring�- Fire Rends � Mages STREAK � FIRE MINIONS  
6b�- Matties/Lugians -�Ring�- Fire/Light Rends  � Mages STREAK � FIRE MINIONS  
  
### Arena 7
7a�- Eaters -�Ring�� Soulbound � Mages STREAK / RING � FIRE/LIGHT MINIONS  
7b�- Viamonts -�Ring�- Light Rends  � Mages STREAK � FIRE/LIGHT MINIONS  
  
### Arena 8
8a�- Dillos/Zefirs -�South�- Slash Rends - IMPERILS � LIGHT MINIONS  
8b�- Crystals -�Spread�� Blunt � IMPERILS only on SHARDS � MELEES ON LORD  
  
### Arena 9
9a�- Olthoi -�South�- Soulbound / Olthoi Slayers  � FROST MINIONS  
9b�- Shadows ��Spread �- Shadowfire/Fire Rend � FIRE MINIONS  
  
### Arena 10
10a�- Sleeches -�Spread�- SB / Blunt Rends � Mages STREAK � Light / Frost Minions  
10b�- Ninjas -�South�- Fire/Acid Rends (weepings also work) � ACID/LIGHT/FIRE MINIONS  
  
### Arena 11
11a�- Virindi/Tuskers -�South�- SB/Fire BEST � VIRINDI�s IMPERILS (for minion, melee & archer dmg) � no Imps on Tuskers  
11b�- Ghosts -�Spread�- Soulbound (SB BEST) � Mages STREAK � Fire   
  
### Arena 12
12a�- Ruschk -�South�- Blunt/SB Rends, IMPERILS  
12b�- Mukkir -�South�- Mukkir Slayers (SB/Blunt/SLAYER), IMPERIL  
  
### Arena 13 � UNIQUE TRANSITION ROOM
13a�- Carenzi/Virindi -�South�- Frost/Pierce /Slash, IMPERIL ALL &  NO V vulns � SB�s (LUGIES FOLLOW then URSUIN)  
13b�- Lugians -�South�- Light Rends  
13c & d�- Ursuin -�South�- Fire Rends (TWO TIMES THROUGH)  
  
### Arena 14
14a�- Virindi -�South��SB Excellent SOULBOUND, � IMPERIL ALL plus VULN  
14b�- Olthoi -�South�- Olthoi Slayers � SLAYERS / SOULBOUNDS  
  
### Arena 15
15a�- Mosswarts -�South�- Fire Rends - IMPERILS  
15b�- Falatacot -� South�- Undead slayers / Fire / SB / Light - IMPERILS  
  
### Arena 16
16a�- Viamont Hands -� South�- Light Rends � STREAKS no IMPS  
16b�- Demon Olthoi -�SE Corner�� Olthoi Slayer / Soulbound, IMPERILS, SOUTHEAST CORNER ALL  
  
### Arena 17
17a�- Virindi/Tuskers -�South�- Slash/Fire, 1-2 melee/archers VIRINDI NO vuln, IMPERILS only  
17b�- Penguins -�South�- Fire Rend, lense IMP  
  
### Arena 18
18a�- TMs -�Post�- Blunt/Frost/Slash, ALL TO SOUTHEAST CORNER (Mage�s with TF.. once bludg vuln�d SB yields highest dmg)  
18b�- Elite Guardians -�By the Door�- CS/AR Frost  
Note:�First spawn is 1 Elite, then 3 Elites, then 6 Elites  