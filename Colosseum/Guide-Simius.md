# Colosseum Guide.
This guide is loosely based on Mystic Magesitc's colo guide, which in turn is based on Shedao Shai's Colosseum Walkthrough - Standard Fellow..  
The guide is not propperly tested, edited or perfected yet.  
  
# Setup

## Imper
Have one char (high life) designated Imper. There is a few rooms where more than one char is needed to imp, but they are few.
Imper imps everying before starting to do damage.
Ofcourse, imps are only used in the rooms where imps are needed (see calls).

## Vulner
Have one char (high life) designated Vulner.
Imper vulns everything before starting to do damage.
Ofcourse, vulns are only used in the rooms where vulns are needed (see calls).

## Posts
Posts are used in Monouga room to keep monougas at distance from mages to tufi.

# Rooms  

## Room 6
### a
Mosswarts - Ring - FIRE REND. Mages STREAK. Summons: FIRE.
### b
Matties/Lugians - Ring - FIRE/LIGHT REND. Mages STREAK. Summons: FIRE.

## Room 7
### a
Eaters - Ring. SOULBOUND. MAGES RING/STREAK. Summons: LIGHT.
### b
Viamonts - Ring. LIGHT REND. Mages STREAK. Summons: LIGHT.

## Room 8
### a
Dillos/Zefirs - South - SLASHREND - IMPERILS. Summons: LIGHT.
### b
Crystals - Spread - PIERC/BLUDGE. IMPERILS ON SHARDS !!!MAGES DONT ATTACK LORDS!!!.

## Room 9
### a
Olthoi - South. PIERC/BLUDGE (OLTHOI SLAYER). Mages STREAK. Summons: FROST.
### b
Shadows - Spread. FIRE (SHADOWFIRE or REND). Mages STREAK. Summons: FIRE.

## Room 10
### a
Sleeches - Spread. SOULBOUND OR BLUDGEREND. Mages STREAK. Summons: LIGHT/FROST.
### b
Ninjas - North - FIRE/ACID REND. Summons: FIRE/ACID.

## Room 11
### a
Virindi/Tuskers - South. SOULBOUND/FIRE REND (SING on Virindi). VULN VIRINDIS. Summons: FIRE.
### b
Ghosts - Spread. SOULBOUND/BLUDGE REND. Mages STREAK. Summons: FIRE.

## Room 12
### a
Ruschk - North. BLUDGE REND/SOULBOUND. IMPERILS.
### b
Mukkir - North. BLUDGE/SOULBOUND/SLAYER. IMPERILS.  
  
## Room 13
### a
Carenzi/Virindi - North. FROST/PIERC/SLASH. IMPERIL.
### b
Lugies - North. ALL MAGES IMP - LIGHT REND. Summons: LIGHT.  
### c/d
Ursin - North. ALL MAGES IMP - FIRE REND. Summons: FIRE  
  
## Room 14
### a
Virindi - North. VIRINDI SLAYER/SOULBOUND - VULN AND IMP. Mages STREAK. Summons: FIRE.  
### b
Olthoi - North. PIERC/BLUDGE (OLTHOI SLAYER). Summons: FROST.  
  
## Room 15
### a
Mosswarts - North. FIRE REND. IMPERILS. Summons: FIRE.
### b
Falatacot - North. UNDEAD SLAYER/SOULBOUND. IMPERILS. Summons: FIRE.  
  
## Room 16
### a
Viamonts - North. LIGHT REND. Mages STREAK. Summons: LIGHT.  
### b
Demon Olthoi - NE Corner + Rings. Mages RING/STREAK. OLTHOI SLAYER. Summons: FROST.  
  
## Room 17
### a
Virindi/Tuskers - North. Slash(sb)/Fire(ts). VULN. Summons: FIRE.  
### b
Penguins - North. FIRE REND. IMPERIL. Summons: FIRE.  
  
## Room 18
### a
TMs - ALL SE CORNER - POST BEFORE BELL. FROST/SOULBOUND/TUFI. VULN(Frost and bludge) & IMP. Summons: FROST.  
### b
Elite Guardians - DOOR/NORTH. CS/AR FROST. IMP & VULN. Summons: FROST. 3 Spawns.



